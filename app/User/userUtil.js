/** @format */

const User = require("../../models/User");

exports.getUserByUsername = async (username, deletePassword) => {
  let user = await User.findOne({ username });
  if (user) {
    if (deletePassword) user.password = undefined;
    return user;
  } else {
    return false;
  }
};

exports.getUserByMobile = async (mobile, deletePassword) => {
  let user = await User.findOne({ mobile });
  if (user) {
    if (deletePassword) user.password = undefined;
    return user;
  } else {
    return false;
  }
};

exports.getUserById = async (id, deletePassword) => {
  let user = await User.findById(id);
  if (user) {
    if (deletePassword) user.password = undefined;
    return user;
  } else {
    return false;
  }
};
