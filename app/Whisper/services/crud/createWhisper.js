/** @format */

const Whisper = require("../../../../models/Whisper");
const { checkAudioFile } = require("../../../Shared/files");
const { uploadToS3 } = require("../../../Shared/aws");

exports.createWhisperService = async ({ user, file, body }) => {
  const { hashtags, tags } = body;
  if (!file) {
    return {
      error: "Audio is required",
      status: 400,
    };
  }
  const { error, status, extension } = checkAudioFile(file);
  if (error) {
    return {
      error,
      status,
    };
  }

  let audio = await uploadToS3(file.buffer, extension);
  if (audio.message) {
    return { error: audio.message, status: 400 };
  }
  let whisper = new Whisper({
    audio,
    author: user.id,
    hashtags,
    tags,
  });

  (await whisper.save()).populate("author");
  return {
    message: "Whisper has been added",
    data: whisper,
  };
};
