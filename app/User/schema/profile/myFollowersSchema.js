/** @format */

const Joi = require("joi");

exports.myFollowersSchema = {
  page: Joi.number().integer().positive(),
  limit: Joi.number().integer().positive(),
};
