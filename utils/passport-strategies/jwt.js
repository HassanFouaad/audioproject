/** @format */

const { Strategy } = require("passport-jwt");
const passport = require("passport");

const { ServerError } = require("../core");
const { getUserById } = require("../../app/User/userUtil");

const JWTStrategy = Strategy;

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((obj, done) => {
  done(null, obj);
});

const strategyOptions = {
  jwtFromRequest: (req) => req.get("Authorization"),
  secretOrKey: process.env.JWT_SECRET,
  passReqToCallback: true,
};

const verifyCallback = async (req, jwtPayload, done) => {
  let user = await getUserById(jwtPayload.user._id);
  if (!user) {
    return done(new ServerError("Your account has been deleted", 404));
  }

  let newUser = { ...user._doc, id: user._doc._id };

  req.user = newUser;
  return done(null, user);
};

exports.jwtStrategy = new JWTStrategy(strategyOptions, verifyCallback);
