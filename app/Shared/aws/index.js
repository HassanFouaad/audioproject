/** @format */
const AWS = require("aws-sdk");
const { v5: uuidv5 } = require("uuid");
const { logger } = require("../../../config/logger");
const dotenv = require("dotenv");

const envFound = dotenv.config();
if (!envFound) {
  throw new Error(" Couldn't find .env file!");
}
const AWSconfig = {
  region: process.env.AWS_BUCKET_REGION,
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  Bucket: process.env.S3_BUCKET_NAME,
  signatureVersion: "v4",
  logger: console,
  httpOptions: {
    connectTimeout: 2 * 5000, // time succeed in starting the call
    timeout: 2 * 5000, // time to wait for a response
  },
};

const s3 = new AWS.S3(AWSconfig.region ? AWSconfig : null);
exports.uploadToS3 = async (file, ext) => {
  const hashedFileName = uuidv5(`${file}${Date.now()}`, uuidv5.URL);
  const params = {
    Bucket: AWSconfig.Bucket,
    Body: file,
    ACL: "public-read",
    Key: `${hashedFileName}.${ext}`,
  };

  try {
    const { Location } = await s3.upload(params).promise();
    return Location;
  } catch (error) {
    logger.error(error);
    return {
      message: "Unable to upload image",
    };
  }
};

exports.getFileKeyFromS3Location = (fileLocation) => {
  return fileLocation.split("amazonaws.com/")[1];
};

exports.deleteFromS3 = async (imageName, config = AWSconfig) => {
  const s3 = new AWS.S3(config.region ? config : null);
  const params = {
    Bucket: config.Bucket,
    Key: imageName,
  };

  try {
    const { $response } = await s3.deleteObject(params).promise();
    return $response;
  } catch (error) {
    logger.error(error);
    return new Error(error.message);
  }
};
