/** @format */
const dotenv = require("dotenv");

const envFound = dotenv.config();
if (!envFound) {
  throw new Error(" Couldn't find .env file!");
}

module.exports = {
  port: process.env.PORT,
  mongoDBConnection: process.env.MONGODB_CONNECTION_STRING,

  api: {
    prefix: "/",
  },
  serverUrl: process.env.SERVER_URL,

  pagination: {
    pageSize: 10,
  },

  nodeMailerEmail: process.env.MAILER_EMAIL,

  nodeMailerPassword: process.env.MAILER_PASS,

  IMAGE_FILE_SIZE: 10000000,
  AUDIO_FILE_SIZE: 10000000,
  FILE_TYPES_IMAGE: ".jpg#.jpeg#.png#.gif",
  FILE_TYPES_AUDIO: ".mp3#.mpeg",
};
