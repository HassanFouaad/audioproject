/** @format */

const { ServerError } = require("../utils/core");

exports.controller = (service) => async (req, res, next) => {
  const { error, status, message, meta, data } = await service(req);
  if (error) return next(new ServerError(error, status));
  return res.json({ message, meta, data });
};
