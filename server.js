/** @format */

const http = require("http");

const { logger } = require("./config/logger");

const { port, serverUrl } = require("./config");

require("./config/db");

const { app } = require("./app");

const httpServer = http.createServer(app);

httpServer.listen(port, () =>
  logger.info(`Server is up on ${serverUrl}:${port}`)
);
