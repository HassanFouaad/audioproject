/** @format */

const jwt = require("jsonwebtoken");
require("dotenv").config();

const signToken = (user) => {
  const token = jwt.sign({ user }, process.env.JWT_SECRET, {
    expiresIn: process.env.TOKEN_EXPIRY,
  });
  return token;
};

module.exports = {
  signToken,
};
