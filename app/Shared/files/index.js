/** @format */

const {
  AUDIO_FILE_SIZE,
  IMAGE_FILE_SIZE,
  FILE_TYPES_IMAGE,
  FILE_TYPES_AUDIO,
} = require("../../../config");

exports.checkImgFile = (file) => {
  const [, fileExt] = file.mimetype.split("/");
  const imageTypes = FILE_TYPES_IMAGE.split("#");
  let mediaType = "file";

  if (imageTypes.includes(`.${fileExt}`)) {
    mediaType = "image";
  } else {
    return {
      error: `The file Extention .${fileExt} is not accepted`,
      status: 400,
    };
  }

  if (mediaType === "image" && file.size > IMAGE_FILE_SIZE) {
    return {
      message: `${fileExt} files should be less than ${IMAGE_FILE_SIZE} bytes`,
      status: 400,
    };
  }

  return { extension: fileExt };
};

exports.checkAudioFile = (file) => {
  const [, fileExt] = file.mimetype.split("/");

  const audioTypes = FILE_TYPES_AUDIO.split("#");

  let audioType = "file";

  if (audioTypes.includes(`.${fileExt}`)) {
    audioType = "audio";
  } else {
    return {
      error: `The file Extention .${fileExt} is not accepted`,
      status: 400,
    };
  }

  if (audioType === "audio" && file.size > AUDIO_FILE_SIZE) {
    return {
      message: `${fileExt} files should be less than ${AUDIO_FILE_SIZE} bytes`,
      status: 400,
    };
  }
  return { extension: fileExt };
};
