/** @format */

const pagination = async (page, limit, model, query, aggregation, populate) => {
  if (page || limit) {
    if (!page || !limit) {
      return {
        error: "Page and limit both are required",
      };
    }
  }
  if (!page) {
    page = 1;
  }

  if (!limit) {
    limit = 10;
  }

  let result = await model
    .find(query, aggregation)
    .sort({ _id: 1 })
    .skip(page > 0 ? (page - 1) * limit : 0)
    .limit(limit)
    .populate(
      "followers.user",
      "id username firstname lastname mobileNumber profilePicture"
    );

  return {
    result,
  };
};
module.exports = {
  pagination,
};
