/** @format */
const dotenv = require("dotenv");

const envFound = dotenv.config();
if (!envFound) {
  throw new Error(" Couldn't find .env file!");
}

const mongoose = require("mongoose");

const { mongoDBConnection } = require(".");

const { logger } = require("./logger");

const connectToDB = async () => {
  try {
    await mongoose.connect(mongoDBConnection, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    });

    mongoose.Promise = global.Promise;
    mongoose.set("useFindAndModify", false);
    mongoose.set("useCreateIndex", true);
    mongoose.set("autoIndex", false);

    logger.info("Connected To MongoDB");
  } catch (error) {
    logger.error(error.message);
  }
};
module.exports = connectToDB();
