/** @format */
const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

exports.followUserSchema = {
  userId: Joi.objectId().optional().required(),
};
