/** @format */

const Joi = require("joi");

exports.updateProfileSchema = {
  username: Joi.string().min(3).max(30).optional(),
  firstname: Joi.string().alphanum().min(3).max(30).optional(),
  lastname: Joi.string().alphanum().min(3).max(30).optional(),
  gender: Joi.string().valid("male", "female").optional(),
  about: Joi.string().min(3).max(30).optional(),
  status: Joi.string().valid("online", "offline").optional(),
};
