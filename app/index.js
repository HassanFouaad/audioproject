/** @format */

const express = require("express");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const cors = require("cors");
const trimRequest = require("trim-request");
const logger = require("morgan");
const debug = require("debug")("app:express");
const passport = require("passport");
const { rateLimiter } = require("../middlewares/rateLimiter");
const { userRouter } = require("./User/router");
const { whisperRouter } = require("./Whisper/router");
const multer = require("multer");
const { ServerError } = require("../utils/core");
//Define App
const app = express();

//Applying Middlewares

app.use(helmet());
app.use(cors());
app.use(bodyParser.json({ limit: "50mb", strict: false }));
app.use(trimRequest.all);
app.use(logger("dev"));
app.use(rateLimiter);
app.use(passport.initialize());
app.use(multer().single("fileName"));
//Running
app.get("/", (req, res, next) => {
  res.json({ message: "Server is Up and Running!" });
});

///Routes
app.use("/user", userRouter);
app.use("/whisper", whisperRouter);

// 404 handler
app.use("*", (req, res, next) => {
  next(new ServerError("API_NOT_FOUND", 404));
});

// error handler
app.use((err, req, res, next) => {
  if (!err.status) {
    console.log(err);
    return res.status(500).json({ message: "Server Error" });
  }

  debug("Custom Server Error >", err.message);
  return res
    .status(err.status)
    .json({ message: err.message, status: err.status });
});

module.exports = { app };
