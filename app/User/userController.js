/** @format */

const { createUserService } = require("./services/crud/createUserService");
const { loginService } = require("./services/crud/loginService");
const { controller } = require("../../middlewares/controller");

const {
  updateProfileService,
} = require("./services/crud/updateProfileService");

const {
  followUserService,
  myFollowersService,
  myFollowingService,
} = require("./services/follow/followUser");

module.exports = {
  createUserController: controller(createUserService),
  loginController: controller(loginService),
  updateProfileController: controller(updateProfileService),
  followUserController: controller(followUserService),
  myFollowersController: controller(myFollowersService),
  myFollowingController: controller(myFollowingService),
};
