/** @format */
const User = require("../../../../models/User");
const { getUserByUsername } = require("../../userUtil");

exports.updateProfileService = async ({ user: { id: userId }, body }) => {
  const user = await User.findById(userId);
  const username = body.username;
  if (username != user.username) {
    let takenusername = await getUserByUsername(body.username);
    if (takenusername) {
      return {
        message: "Username is taken",
        status: 400,
      };
    }
  }
  await user.update(body);
  user.password = undefined;

  return {
    data: user,
    message: "Profile has been updates",
  };
};
