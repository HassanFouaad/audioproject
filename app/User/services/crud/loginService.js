/** @format */

const { verifyPassword } = require("../../../../utils/password");

const { signToken } = require("../../../../utils/token");

const { getUserByMobile, getUserByUsername } = require("../../userUtil");

exports.loginService = async ({ body }) => {
  const { loginData, password } = body;

  const userMobileFound = await getUserByMobile(loginData, false);

  const userUsernameFound = await getUserByUsername(loginData, false);

  let user = null;

  if (userMobileFound) {
    user = userMobileFound;
  }

  if (userUsernameFound) {
    user = userUsernameFound;
  }

  if (!user) {
    return {
      error: "Invalid credentials",
      status: 401,
    };
  }

  const validPassword = await verifyPassword(password, user.password);

  if (!validPassword) return { error: "Invalid credentials", status: 401 };

  user.password = undefined;
  user.__v = undefined;
  user.activityStatus = "online";

  return {
    message: "Welcome Back",
    data: { user, token: signToken(user) },
  };
};
