/** @format */
const User = require("../../../../models/User");

const { hashPassword } = require("../../../../utils/password");

const Following = require("../../../../models/Following");
const Followers = require("../../../../models/Followers");

const { getUserByMobile, getUserByUsername } = require("../../userUtil");

exports.createUserService = async ({ body }) => {
  const {
    username,
    firstname,
    lastname,
    gender,
    mobileNumber,
    password,
  } = body;

  const mobileTaken = await getUserByMobile(mobileNumber);

  const usernameTaken = await getUserByUsername(username);

  if (mobileTaken) {
    return {
      error: "Mobile is taken",
      status: 400,
    };
  }

  if (usernameTaken) {
    return {
      error: "Username is taken",
      status: 400,
    };
  }

  const hashedPassword = await hashPassword(password);

  let user = new User({
    username,
    firstname,
    lastname,
    gender,
    mobileNumber,
    password: hashedPassword,
    status: "online",
  });

  await new Following({ user: user.id }).save();
  await new Followers({ user: user.id }).save();

  try {
    await user.save();
    user.password = undefined;
    return {
      message: "You have created your account succesfully",
      data: user,
    };
  } catch (error) {
    console.log(error);
  }
};
