/** @format */

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const whisperSchema = new mongoose.Schema({
  audio: {
    type: String,
    required: "Please record audio",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: "You must supply an author",
  },
  hashtags: {
    type: Array,
    default: [],
  },
  tags: {
    type: Array,
    default: [],
  },
});

module.exports = mongoose.model("Whisper", whisperSchema);
