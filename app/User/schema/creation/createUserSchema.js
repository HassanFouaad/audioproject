/** @format */

const Joi = require("joi");

exports.createUserSchema = {
  username: Joi.string().min(3).max(30).required(),
  firstname: Joi.string().alphanum().min(3).max(30).required(),
  lastname: Joi.string().alphanum().min(3).max(30).required(),
  mobileNumber: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().alphanum().min(3).max(30).required(),
  gender: Joi.string().valid("male", "female").required(),
};
