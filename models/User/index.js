/** @format */

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    firstname: {
      type: String,
      trim: true,
      required: true,
      maxLength: 20,
    },

    lastname: {
      type: String,
      trim: true,
      required: true,
      maxLength: 20,
    },

    username: {
      type: String,
      trim: true,
      required: true,
      maxLength: 20,
      unique: "This username is taken!",
    },

    about: {
      type: String,
      trim: true,
    },

    gender: {
      type: String,
      trim: true,
    },

    profilePicture: {
      type: String,
      trim: true,
    },

    mobileNumber: {
      type: String,
      trim: true,
      required: true,
      unique: "This mobile is Taken!",
    },

    activityStatus: {
      type: String,
      default: "offline",
    },

    password: {
      type: String,
    },

    deletedAt: {
      type: Date,
    },

    activated: {
      type: Boolean,
    },
  },
  { timestamps: true }
);

module.exports = User = mongoose.model("User", userSchema);
