/** @format */

const Joi = require("joi");

exports.createWhisperSchema = {
  hashtags: Joi.array().items(Joi.string().min(3).max(30)).optional(),
  tags: Joi.array().items(Joi.string().min(3).max(30)).optional(),
};
