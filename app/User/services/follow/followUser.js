/** @format */

const User = require("../../../../models/User");
const ChatRoom = require("../../../../models/Chat");
const Following = require("../../../../models/Following");
const Followers = require("../../../../models/Followers");

exports.followUserService = async ({ user, body }) => {
  const { userId } = body;

  if (userId == user.id) {
    return {
      error: "Failed to follow",
      status: 400,
    };
  }
  const userToBeFollowed = await User.findById(userId);

  if (!userToBeFollowed) {
    return {
      error: "Invalid user id",
      status: 404,
    };
  }

  let updateFollowing = await Following.updateOne(
    {
      user: user.id,
      "following.user": { $ne: body.userId },
    },
    {
      $addToSet: { following: { user: body.userId } },
    }
  );

  if (updateFollowing.nModified == 1) {
    await Followers.updateOne(
      {
        user: body.userId,
      },
      {
        $push: { followers: { user: user.id } },
      }
    );

    return {
      message: "Followed",
    };
  }

  if (updateFollowing.nModified != 1) {
    await Followers.updateOne(
      {
        user: body.userId,
      },
      {
        $pull: { followers: { user: user.id } },
      }
    );

    await Following.updateOne(
      { user: user.id },
      {
        $pull: { following: { user: body.userId } },
      }
    );

    return {
      message: "Unfollowed",
    };
  }
};

exports.myFollowersService = async ({ query: { page, limit }, user }) => {
  if (page || limit) {
    if (!page || !limit) {
      return {
        error: "Page and limit both are required",
        status: 400,
      };
    }
  }

  const defaultLimit = limit ? parseInt(limit) : 10;
  const defaultPage = page ? parseInt(page) : 0;

  let result = await Followers.aggregate([
    { $match: { user: user.id } },
    { $unwind: "$followers" },
    {
      $lookup: {
        from: "users",
        foreignField: "_id",
        localField: "followers.user",
        as: "user",
      },
    },
    {
      $lookup: {
        from: "users",
        foreignField: "_id",
        localField: "followers.user",
        as: "allCount",
      },
    },
    { $sort: { "followers.createdAt": -1 } },
    { $skip: defaultLimit * (defaultPage - 1) },
    { $limit: defaultLimit },
    { $group: { _id: "$_id", followers: { $push: "$user" } } },
    {
      $project: {
        followers: 1,
      },
    },
  ]);

  if (result[0]) {
    result = await result[0].followers.map((f) => {
      let user = f[0];
      return {
        _id: user._id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        gender: user.gender,
        mobileNumber: user.mobileNumber,
      };
    });
  }

  return {
    message: "Success",
    data: result,
  };
};

exports.myFollowingService = async ({ query: { page, limit }, user }) => {
  if (page || limit) {
    if (!page || !limit) {
      return {
        error: "Page and limit both are required",
        status: 400,
      };
    }
  }

  const defaultLimit = limit ? parseInt(limit) : 10;
  const defaultPage = page ? parseInt(page) : 0;

  let result = await Following.aggregate([
    { $match: { user: user.id } },
    { $unwind: "$following" },
    {
      $lookup: {
        from: "users",
        foreignField: "_id",
        localField: "following.user",
        as: "user",
      },
    },
    { $sort: { "following.createdAt": -1 } },
    { $skip: defaultLimit * (defaultPage - 1) },
    { $limit: defaultLimit },
    { $group: { _id: "$_id", following: { $push: "$user" } } },
  ]);

  if (result[0]) {
    result = await result[0].following.map((f) => {
      let user = f[0];
      return {
        _id: user._id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        gender: user.gender,
        mobileNumber: user.mobileNumber,
      };
    });
  }

  return {
    message: "Success",
    data: result,
  };
};
