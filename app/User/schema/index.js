/** @format */

const { createUserSchema } = require("./creation/createUserSchema");

const { loginSchema } = require("./login/loginSchema");
const { followUserSchema } = require("./profile/followUserSchema");
const { updateProfileSchema } = require("./profile/updateUserProfileSchema");
const { myFollowersSchema } = require("./profile/myFollowersSchema");

module.exports = {
  createUserSchema,
  loginSchema,
  updateProfileSchema,
  followUserSchema,
  myFollowersSchema,
};
