/**
 * /* eslint-disable global-require
 *
 * @format
 */

const passport = require("passport");

const { jwtStrategy } = require("./jwt");

passport.use(jwtStrategy);

const availableStrategies = {
  passport,
  jwt: (options = {}) => (req, res, next) => {
    passport.authenticate("jwt", options, (err, user, info) => {
      if (err) {
        const errorMsg = err.toString().split(":").pop();
        return res.status(401).json({
          message: `Unauthorized,${errorMsg || " check you're token"}`,
        });
      }
      if (!user || info) {
        return res
          .status(401)
          .json({ message: `Unauthorized, check you\'re token` });
      }
      return next();
    })(req, res, next);
  },
  facebook: () => {},
  google: () => {},
  facebookToken: () => {},
  googleToken: () => {},
};

if (JSON.parse(process.env.SOCIAL_AVAIL)) {
  const { facebookStrategy } = require("./web/facebook");
  const { googleStrategy } = require("./web/google");
  const { facebookTokenStrategy } = require("./mobile-web/facebook");
  const { googleTokenStrategy } = require("./mobile-web/google");

  passport.use(facebookStrategy);
  passport.use(googleStrategy);
  passport.use(facebookTokenStrategy);
  passport.use(googleTokenStrategy);

  availableStrategies.facebook = (options = {}) =>
    passport.authenticate("facebook", options);
  availableStrategies.google = (options = {}) =>
    passport.authenticate("google", options);
  availableStrategies.facebookToken = (options = {}) =>
    passport.authenticate("facebook-token", options);
  availableStrategies.googleToken = (options = {}) =>
    passport.authenticate("google-token", options);
}

module.exports = availableStrategies;
