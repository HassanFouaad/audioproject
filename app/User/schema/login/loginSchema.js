/** @format */

const Joi = require("joi");

exports.loginSchema = {
  loginData: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().alphanum().min(8).max(30).required(),
};
