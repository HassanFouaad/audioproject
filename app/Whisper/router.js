/** @format */

const router = require("express-promise-router")();

const validate = require("../../middlewares/validator");

const { jwt } = require("../../utils/passport-strategies");

const { createWhisperSchema } = require("./schema");

const { createWhisperController } = require("./whisperController");

router.post("/", jwt(), validate(createWhisperSchema), createWhisperController);

exports.whisperRouter = router;
