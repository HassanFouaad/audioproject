/** @format */

const router = require("express-promise-router")();

const validate = require("../../middlewares/validator");

const { jwt } = require("../../utils/passport-strategies");

const {
  createUserSchema,
  loginSchema,
  updateProfileSchema,
  followUserSchema,
  myFollowersSchema,
} = require("./schema");

const {
  createUserController,
  loginController,
  updateProfileController,
  followUserController,
  myFollowersController,
  myFollowingController,
} = require("./userController");

router.post("/create", validate(createUserSchema), createUserController);

router.post("/login", validate(loginSchema), loginController);

router.post(
  "/update",
  jwt(),
  validate(updateProfileSchema),
  updateProfileController
);

router.post("/follow", jwt(), validate(followUserSchema), followUserController);

router.get(
  "/my-followers",
  jwt(),
  validate(myFollowersSchema),
  myFollowersController
);

router.get(
  "/my-following",
  jwt(),
  validate(myFollowersSchema),
  myFollowingController
);

exports.userRouter = router;
