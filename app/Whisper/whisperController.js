/** @format */

const { controller } = require("../../middlewares/controller");
const { createWhisperService } = require("./services/crud/createWhisper");

module.exports = {
  createWhisperController: controller(createWhisperService),
};
